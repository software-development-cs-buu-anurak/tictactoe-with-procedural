package com.anurak;

import java.util.Arrays;
import java.util.Scanner;

/**
 * <pre>
 * Main Application:
 * Procedural Paradigm TicTacToe Program
 * 
 * 63160015 Anurak Yutthanawa
 * B.Sc. Computer Science, Faculty of Informatics
 * Burapha University
 * 
 * 2nd projects: TicTacToe
 * Software Development
 * 
 * Create on MAVEN package in Visual Studio Code
 * </pre>
 */

public class App 
{
    /**
     * static variable in class App for get input from user in terminal
     */
    static final Scanner scanner = new Scanner( System.in );


    /**
     * Round use to keep tracking who's turn
     */
    static int round = 0;


    /**
     * Table use to represent the tic tac toe 3*3 table
     */
    static char[][] table = { { '-', '-', '-' }, { '-', '-', '-' }, { '-', '-', '-' } };


    /**
     * isFinish use to tell the win statement to main {@link(App.java#L55)} 
     */
    static Boolean isFinish = false;


    /**
     * Main program
     * @param args
     */
    public static void main( String[] args )
    {
        printWelcome();
        printTable( table );

        while ( !isFinish )
        /**
         * Loop for each turn
         */
        {
            printTurn( round );
            table = inputTable( round, table );
            printTable( table );
            isFinish = checkState( round, table );
            round++;
        }
    }

    /**
     * Check Win or Draw Statement
     * @param round (int)
     * @param table2
     * @return Win or Draw Statements
     */
    protected static Boolean checkState( int round, char[][] table )
    {
        return checkWin( round, table ) || checkDraw();
    }

    /**
     * @return Draw or Not Statements
     */
    private static boolean checkDraw()
    {
        Boolean result = checkDrawCondition();
        if ( result ) showDraw();
        return result;
    }

    /**
     * Show winning statement
     */
    private static void showDraw()
    {
        System.out.printf( ">>>Draw<<<\n" );
    }

    /**
     * Method to check all draw condition
     * @return Draw or Not Statements
     */
    private static boolean checkDrawCondition()
    {
        return  !Arrays.deepToString( table ).contains( "-" ) ;
    }

    /**
     * Ask user for input row column
     * @param round (int)
     * @param table TicTacToe: table(char[][])
     * @return table(char[][])
     */
    protected static char[][] inputTable( int round, char[][] table )
    {
        System.out.println( "Please input row, col:" );
        table = setTable( ( round % 2 == 0 ) ? 'O' : 'X', scanner.nextInt(), scanner.nextInt(), table );
        return table;
    }

    /**
     * Set user's row and column choosen to table
     * @param player (char: 'X' , 'O')
     * @param row User input: Row
     * @param column User input: Column
     * @param table TicTacToe: table(char[][])
     * @return table(char[][])
     */
    private static char[][] setTable( char player, int row, int column, char[][] table )
    {
        table[--row][--column] = player;
        return table;
    }

    /**
     * Print turn round
     * @param round (int)
     */
    private static void printTurn( int round ) 
    {
        System.out.printf( "Turn %c\n", ( round % 2 == 0 ) ? 'O' : 'X' );
    }

    /**
     * Print Welcome letter for starting game scenario
     */
    private static void printWelcome()
    {
        System.out.println( "Welcome to OX Game" );
    }

    /**
     * Print table
     * @param table2
     */
    private static void printTable( char[][] table )
    {
        System.out.printf( "%c %c %c\n%c %c %c\n%c %c %c\n", 
            table[0][0], table[0][1], table[0][2], 
            table[1][0], table[1][1], table[1][2],
            table[2][0], table[2][1], table[2][2]
            );
    }

    /**
     * Method to check win condition
     * @param round (int)
     * @param table2
     * @return Win or Not Statements
     */
    private static Boolean checkWin( int round, char[][] table )
    {
        char player = ( round % 2 == 0 ) ? 'O' : 'X';
        Boolean result = checkWinCondition( player, table );
        if ( result ) showWin( player );
        return result;
    }

    /**
     * Show winning statement
     * @param player (char: 'X' , 'O')
     */
    private static void showWin( char player ) 
    {
        System.out.printf( ">>>%c Win<<<\n", player );
    }

    /**
     * Method to check all win condition
     * @param player (char: 'X' , 'O')
     * @param table2
     * @return Win or Not Statements
     */
    private static Boolean checkWinCondition( char player, char[][] table )
    {
        return  checkVertical( player, table )     ||
                checkHorizontal( player, table )   ||
                checkDiagonal( player, table )     ;
    }

    /**
     * Method to check win condition in vertical axis
     * @param player (char: 'X' , 'O')
     * @param table2
     * @return Win or Not Statements
     */
    private static Boolean checkVertical( char player, char[][] table )
    {
        return ( table[0][0] == table[1][0] ) && ( table[2][0] == table[1][0] ) && ( table[2][0] == player ) || 
               ( table[0][1] == table[1][1] ) && ( table[2][1] == table[1][1] ) && ( table[2][1] == player ) ||
               ( table[0][2] == table[1][2] ) && ( table[2][2] == table[1][2] ) && ( table[2][2] == player ) ;
    }

    /**
     * Method to check win condition in horizontal axis
     * @param player (char: 'X' , 'O')
     * @param table2
     * @return Win or Not Statements
     */
    private static Boolean checkHorizontal( char player, char[][] table )
    {
        return ( table[0][0] == table[0][1] ) && ( table[0][2] == table[0][1] ) && ( table[0][0] == player ) || 
               ( table[1][0] == table[1][1] ) && ( table[1][2] == table[1][1] ) && ( table[1][0] == player ) ||
               ( table[2][0] == table[2][1] ) && ( table[2][2] == table[2][1] ) && ( table[2][0] == player ) ;
    }

    /**
     * Method to check win condition in diagonal axis
     * @param player (char: 'X' , 'O')
     * @param table2
     * @return Win or Not Statements
     */
    private static Boolean checkDiagonal( char player, char[][] table )
    {
        return ( table[0][0] == table[1][1] ) && ( table[1][1] == table[2][2] ) && ( table[1][1] == player ) || 
               ( table[0][2] == table[1][1] ) && ( table[1][1] == table[2][0] ) && ( table[1][1] == player ) ;
    }
}
