# TicTacToe Program with Procedural Daradigm

License: [MIT](LICENSE)

Developer: Anurak Yuthanawa

## Introduction
This project is the one of learning material of 88634159 Software Development, Bachelor of Science in Computer Science program, Burapha University, Thailand. 

## Maven Java Projects
You must understand how to install software on your computer. If you do not know how to do this, please ask someone at your office, school, etc. or pay someone to explain this to you. The Maven mailing lists are not the best place to ask for this advice. *[(Maven in 5 Minutes)](https://maven.apache.org/guides/getting-started/maven-in-five-minutes.html)*

## Developing Tools
- Java (openjdk 18-ea 2022-03-22)
- Maven 
- Visual Studio Code 
- Ubuntu 22.04
